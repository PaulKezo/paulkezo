<cfsilent>

<!--- Global XFA --->
    <cfset xfa.prods_main = "prods.main" />
    <cfset xfa.show_save_prod_form = "prods.save_form" />
    <cfset xfa.do_save_prod = "prods.save" />
    <cfset xfa.do_delete_prod = "prods.delete" />
    <cfset xfa.do_view_prod = "prods.view" />

    <cfset xfa.categs_main = "categs.main" />

<!--- Initialize applicaiton manager components. --->
    <cfparam name="url.appReload" type="string" default="false"/>
    <cfif not structKeyExists(application, 'appInitialized') or url.appReload>
        <cflock name="appInitBlock" type="exclusive" timeout="10">
            <cfif not structKeyExists(application, 'appInitialized') or url.appReload>
                <cfset application.prodsmanager = createObject('component', 'coldfusion-fusebox5-test.components.prod.ProdsManager').init()/>
                <cfset application.categsmanager = createObject('component', 'coldfusion-fusebox5-test.components.categ.CategsManager').init()/>
                <cfset application.dataformats = createObject('component', 'coldfusion-fusebox5-test.components.Util.DataFormats').init()/>
                <cfset application.appInitialized = true/>
            </cfif>
        </cflock>
        <cfset structClear(session)/>
    </cfif>

<!--- Set app constants. --->
    <cfset self = "index.cfm">
    <cfset mySelf = "#urlSessionFormat('#self#')#"/>

    <cfif findNoCase('index.cfm;', mySelf)>
        <cfset mySelf = replace(mySelf, 'index.cfm;', 'index.cfm?')/>
    </cfif>

    <cfif right(mySelf, 9) eq "index.cfm">
        <cfset mySelf = mySelf & "?"/>
        <cfelse>
        <cfset mySelf = mySelf & "&"/>
    </cfif>

    <cfset mySelf = mySelf & "#application.fusebox.fuseactionVariable#="/>

</cfsilent>
