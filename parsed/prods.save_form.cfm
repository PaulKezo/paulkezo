<cfsetting enablecfoutputonly="true" />
<cfprocessingdirective pageencoding="utf-8" />
<!--- circuit: prods --->
<!--- fuseaction: save_form --->
<cftry>
<cfset myFusebox.thisPhase = "appinit">
<cfset myFusebox.thisCircuit = "prods">
<cfset myFusebox.thisFuseaction = "save_form">
<cfif myFusebox.applicationStart or
		not myFusebox.getApplication().applicationStarted>
	<cflock name="#application.ApplicationName#_fusebox_#FUSEBOX_APPLICATION_KEY#_appinit" type="exclusive" timeout="30">
		<cfif not myFusebox.getApplication().applicationStarted>
			<cfset myFusebox.getApplication().applicationStarted = true />
		</cfif>
	</cflock>
</cfif>
<!--- do action="m_prods.get_prods_desc" --->
<cfset myFusebox.thisPhase = "requestedFuseaction">
<cfset myFusebox.thisCircuit = "m_prods">
<cfset myFusebox.thisFuseaction = "get_prods_desc">
<cftry>
<cfoutput><cfinclude template="../model/prods/act_get_prods_desc.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 22 and right(cfcatch.MissingFileName,22) is "act_get_prods_desc.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse act_get_prods_desc.cfm in circuit m_prods which does not exist (from fuseaction m_prods.get_prods_desc).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
<!--- do action="m_categs.get_categs" --->
<cfset myFusebox.thisCircuit = "m_categs">
<cfset myFusebox.thisFuseaction = "get_categs">
<cftry>
<cfoutput><cfinclude template="../model/categs/act_get_categs.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 18 and right(cfcatch.MissingFileName,18) is "act_get_categs.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse act_get_categs.cfm in circuit m_categs which does not exist (from fuseaction m_categs.get_categs).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
<cfset myFusebox.thisCircuit = "prods">
<cfset myFusebox.thisFuseaction = "save_form">
<cfif isDefined('url.Prod_id')>
<!--- do action="m_prods.get_prod_by_id" --->
<cfset myFusebox.thisCircuit = "m_prods">
<cfset myFusebox.thisFuseaction = "get_prod_by_id">
<cftry>
<cfoutput><cfinclude template="../model/prods/act_get_prod_by_id.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 22 and right(cfcatch.MissingFileName,22) is "act_get_prod_by_id.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse act_get_prod_by_id.cfm in circuit m_prods which does not exist (from fuseaction m_prods.get_prod_by_id).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
<cfset myFusebox.thisCircuit = "prods">
<cfset myFusebox.thisFuseaction = "save_form">
</cfif>
<!--- do action="v_prods.header" --->
<cfset myFusebox.thisCircuit = "v_prods">
<cfset myFusebox.thisFuseaction = "header">
<cfsavecontent variable="header_content">
<cftry>
<cfoutput><cfinclude template="../view/prods/dsp_header.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 14 and right(cfcatch.MissingFileName,14) is "dsp_header.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse dsp_header.cfm in circuit v_prods which does not exist (from fuseaction v_prods.header).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
</cfsavecontent>
<!--- do action="v_prods.save_form" --->
<cfset myFusebox.thisFuseaction = "save_form">
<cfsavecontent variable="body_content">
<cfif NOT isDefined('url.Prod_id')>
<cftry>
<cfoutput><cfinclude template="../view/prods/frm_add.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 11 and right(cfcatch.MissingFileName,11) is "frm_add.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse frm_add.cfm in circuit v_prods which does not exist (from fuseaction v_prods.save_form).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
<cfelse>
<cftry>
<cfoutput><cfinclude template="../view/prods/frm_edit.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 12 and right(cfcatch.MissingFileName,12) is "frm_edit.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse frm_edit.cfm in circuit v_prods which does not exist (from fuseaction v_prods.save_form).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
</cfif>
</cfsavecontent>
<!--- do action="v_prods.footer" --->
<cfset myFusebox.thisFuseaction = "footer">
<cfsavecontent variable="footer_content">
<cftry>
<cfoutput><cfinclude template="../view/prods/dsp_footer.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 14 and right(cfcatch.MissingFileName,14) is "dsp_footer.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse dsp_footer.cfm in circuit v_prods which does not exist (from fuseaction v_prods.footer).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
</cfsavecontent>
<!--- fuseaction action="v_layouts.buildpage" --->
<cfset myFusebox.thisPhase = "postprocessFuseactions">
<cfset myFusebox.thisCircuit = "v_layouts">
<cfset myFusebox.thisFuseaction = "buildpage">
<cfif not isDefined("layout")><cfset layout = "main" /></cfif>
<cfif (structKeyExists(GetHttpRequestData(),'headers')             AND structKeyExists(GetHttpRequestData()['headers'],'X-Requested-With')             AND GetHttpRequestData()['headers']['X-Requested-With'] EQ 'XMLHttpRequest')>
<cfset layout = "ajax_json" />
</cfif>
<cfif layout EQ 'ajax_json'>
<cftry>
<cfoutput><cfinclude template="../view/layouts/lay_ajax_json.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 17 and right(cfcatch.MissingFileName,17) is "lay_ajax_json.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse lay_ajax_json.cfm in circuit v_layouts which does not exist (from fuseaction v_layouts.buildpage).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
</cfif>
<cfif layout EQ 'main'>
<cftry>
<cfoutput><cfinclude template="../view/layouts/lay_main.cfm"></cfoutput>
<cfcatch type="missingInclude"><cfif len(cfcatch.MissingFileName) gte 12 and right(cfcatch.MissingFileName,12) is "lay_main.cfm">
<cfthrow type="fusebox.missingFuse" message="missing Fuse" detail="You tried to include a fuse lay_main.cfm in circuit v_layouts which does not exist (from fuseaction v_layouts.buildpage).">
<cfelse><cfrethrow></cfif></cfcatch></cftry>
</cfif>
<cfcatch><cfrethrow></cfcatch>
</cftry>

