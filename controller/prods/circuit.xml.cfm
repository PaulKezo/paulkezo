<circuit access="public">

    <prefuseaction>
        <do action="m_prods.get_prods_desc" />
        <do action="m_categs.get_categs" />
    </prefuseaction>
 

    <fuseaction name="main">
        <do action="v_prods.header" contentvariable="header_content" />
        <do action="v_prods.body" contentvariable="body_content" />
        <do action="v_prods.footer" contentvariable="footer_content" />
    </fuseaction>

    <fuseaction name="save_form">
        <if condition="isDefined('url.Prod_id')">
            <true><do action="m_prods.get_prod_by_id" /></true>
        </if>

        <do action="v_prods.header" contentvariable="header_content" />
        <do action="v_prods.save_form" contentvariable="body_content" />
        <do action="v_prods.footer" contentvariable="footer_content" />
    </fuseaction>

    <fuseaction name="save">
        <do action="m_prods.save_prod"/>
    </fuseaction>

    <fuseaction name="view">
        <if condition="isDefined('url.Prod_id')">
            <true>
                <do action="m_prods.get_prod_by_id" />
                <do action="m_categs.get_categs" />
                <do action="v_prods.header" contentvariable="header_content" />
                <do action="v_prods.view_lp" contentvariable="body_content" />
                <do action="v_prods.footer" contentvariable="footer_content" />
            </true>
            <false>
                <relocate url="http://127.0.0.1:8500/coldfusion-fusebox5-test/" />
            </false>
        </if>
    </fuseaction>

    <fuseaction name="delete">
        <do action="m_prods.delete_prod" />
    </fuseaction>

    <fuseaction name="reload_summary_view">
        <do action="m_prods.get_prods_desc" />
        <do action="m_categs.get_categs" />
        <set name="page_content" value = "#application.dataformats.QueryToArrayOfStructures(get_prods_list_desc)#" />
    </fuseaction>

    <fuseaction name="search_keyword_category">
        <do action="m_prods.get_prod_by_keyword_category" />
        <do action="m_categs.get_categs" />
        <set name="page_content" value = "#application.dataformats.QueryToArrayOfStructures(get_prod_by_keyword_category)#" />
    </fuseaction>

    
</circuit>
