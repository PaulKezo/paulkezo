<circuit access="public">

    <prefuseaction>
        <do action="m_prods.get_prods_desc" />
        <do action="m_categs.get_categs" />
    </prefuseaction>

	<fuseaction name="main">
        <do action="v_prods.header" contentvariable="header_content" />
        <do action="v_categs.body" contentvariable="body_content" />
        <do action="v_prods.footer" contentvariable="footer_content" />
	</fuseaction>

</circuit>
