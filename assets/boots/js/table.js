'use strict';

var table = {
  // Header
    head: ['ID', 'First Name', 'Last Name', 'User Name'],
  
  lastId: 3,
  sort: function(column) {

  },
  draw: function() {

    var tbl = $('<table>');
    var head = $('<thead>');
    var body = $('<tbody>');

    var headtr = $('<tr>');

    // Insert all header to head
    for (var i in this.head) {
      var th = $('<th>').text(this.head[i]);
      headtr.append(th);
    }
    head.append(headtr);

    // Insert all data to body
    for (var y in this.data) {
      var tr = $('<tr>');

      if (y % 2 == 1) {
        tr.addClass('even');
      }

      for (var x in this.data[y]) {
        var td = $('<td>').text(this.data[y][x]);
        // Add to tr
        tr.append(td);
      }
      // Add to body
      body.append(tr);
    }

    // Put head to table
    tbl.append(head);
    // Put body to table
    tbl.append(body);

    $('#table').html(tbl);
  },
  create: function(name, gender, position) {
    
    var nextId = ++this.lastId;
    this.data.push([name, gender, position]);
    this.draw();

  },
  delete: function(id) {

    // Find the data with the id
    var find = -1;
    for (var i in this.data) {
      if (this.data[i][0] == id) {
        // Set find
        find = i;
        break;
      }
    }
    // If found
    if (find >= 0) {
      this.data.splice(find, 1);
    }
    // Re draw
    this.draw();

  }
};







