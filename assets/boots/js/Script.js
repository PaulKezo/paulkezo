(function () { 
//The Person Object used to store data in the LocalStorage 
var Person = { 
Id: 0, 
Name: "",  
Email: "", 
Landline: "", 
MobileNo: "", 
Website:"" 
};

//JavaScript object containing methods for LocalStorage management 
var applogic = {
//Clear All Entries, by reading all elements having class as c1 
clearuielements: function () { 
    var inputs = document.getElementsByClassName("c1"); 
    for (i = 0; i < inputs.length; i++) { 
        inputs[i].value = ""; 
    } 
},
//Save Entry in the Localstorage by eading values entered in the 
//UI 
saveitem: function () { 
    var lscount = localStorage.length; //Get the Length of the LocalStorage
    //Read all elements on UI using class name 
    var inputs = document.getElementsByClassName("c1"); 
            Person.Id = inputs[0].value; 
            Person.Name = inputs[1].value; 
            Person.Email = inputs[2].value; 
            Person.Landline = inputs[3].value;  
            Person.MobileNo = inputs[4].value; 
            Person.Website = inputs[5].value; 
    //Convert the object into JSON ans store it in LocalStorage 
            localStorage.setItem("Person_" + lscount, JSON.stringify(Person)); 
    //Reload the Page 
            location.reload(); 
},
//Method to Read Data from the local Storage 
loaddata: function () { 
    var datacount = localStorage.length; 
    if (datacount > 0) 
    { 
        var render = "<table class=table>"; 
        render += "<tr><th>Id</th><th>Name</th><th>Email</th><th>Landline</th><th>MobileNo</th><th>Website</th></tr>"; 
        for (i = 0; i < datacount; i++) { 
            var key = localStorage.key(i); //Get  the Key 
            var person = localStorage.getItem(key); //Get Data from Key 
            var data = JSON.parse(person); //Parse the Data back into the object 
            
            render += "<tr><td>" + data.Id + "</td><td>" + data.Name + " </td>"; 
            render += "<td>" + data.Email + "</td>"; 
            render += "<td>" + data.Landline + "</td>"; 
            render += "<td>" + data.MobileNo + "</td>"; 
            render += "<td>" + data.Website + "</td></tr>"; 
        } 
        render+="</table>"; 
        dvcontainer.innerHTML = render; 
    } 
},
};
//Save object into the localstorage 
var save = document.getElementById('save'); 
save.addEventListener('click', applogic.saveitem, false);
window.onload = function () { 
applogic.loaddata(); 
}; 
})();


$('button.delete').click(function() {

    // ID to delete
    var person = $('input.text.delete').val();
    // Call delete
    table.delete(id);

  });