(function($) {
    $.prods = {
//    form submit
        formSubmit: function() {
            $.ajax({
                url     : '/coldfusion-fusebox5-test/index.cfm?action=prods.save',
                type    : 'POST',
                dataType: 'json',
                data    : $('#save-prod').serialize(),
                success : function(response) {
                    if (response.success) {
                        alert('Success.');
                    } else {
                        alert('Failed.');
                    }
                }
            });
        },
 
 // formSubmit: function() {
 //            $.ajax({
 //                url     : '/coldfusion-fusebox5-test/index.cfm?action=prods.delete&url.Prod_id',
 //                type    : 'POST',
 //                dataType: 'json',
 //                data    : $('#delete-prod').serialize(),
 //                success : function(response) {
 //                    if (response.success) {
 //                        alert('Success.');
 //                    } else {
 //                        alert('Failed.');
 //                    }
 //                }
 //            });
 //        },

        reloadSummaryView: function() {
            var me = this;

            if ($("#search-category").val() != "") {
                me.searchRecord($("#search-category").val(), $("#search-keyword").val())
            } else {
                $.get('/coldfusion-fusebox5-test/index.cfm?action=prods.reload_summary_view', function(data) {
                var jsonData = JSON.parse(data);
                var html = "";

                for (var key in jsonData) {
                    var PROD_ID = jsonData[key].PROD_ID;
                    var CATEG_ID = jsonData[key].CATEG_ID;
                    var PROD_NAME = jsonData[key].PROD_NAME;

                    var edit_prod_cls = "edit-prod";
                    var edit_url = "/coldfusion-fusebox5-test/index.cfm?action=prods.save_form&Prod_id="+PROD_ID;
                    var delete_prod_cls = "delete-prod";
                    var delete_url = "/coldfusion-fusebox5-test/index.cfm?action=prods.delete&Prod_id="+PROD_ID;
                    var view_prod_cls = "view-prod";
                    var view_url = "/coldfusion-fusebox5-test/index.cfm?action=prods.view&Prod_id="+PROD_ID;

                    html += "<tr>";
                    html += "<td class='test'>"+PROD_ID+"</td>";
                    html += "<td>"+get_categ_list[CATEG_ID]+"</td>";
                    html += "<td>"+PROD_NAME+"</td>";
                    html += "<td>";
                    html += "<a class='"+edit_prod_cls+"' rel='"+PROD_ID+"' href='"+edit_url+"'>Edit</a> ";
                    html += "<a class='"+delete_prod_cls+"' rel='"+PROD_ID+"' href='"+delete_url+"'>Delete</a> ";
                    html += "<a class='"+view_prod_cls+"' rel='"+PROD_ID+"' href='"+view_url+"'>Details</a> ";
                    html += "</td>";
                    html += "</tr>";
                }

                $('#grid-data-tbody').html(html);
            });
            }
        },

        deleteRecord: function(Prod_id) {
            $.get('/coldfusion-fusebox5-test/index.cfm?action=prods.delete', {Prod_id: Prod_id}, function(data) {
                var response = JSON.parse(data)

                if (response.success) {
                    $.prods.reloadSummaryView();
                } else {
                    alert('Failed');
                }
            });
        },

        searchRecord: function(category, keyword) {
            $.get('/coldfusion-fusebox5-test/index.cfm?action=prods.search_keyword_category', {category: category, keyword: keyword}, function(data) {
                var jsonData = JSON.parse(data);
                var html = "";

                for (var key in jsonData) {
                    var PROD_ID = jsonData[key].PROD_ID;
                    var CATEG_ID = jsonData[key].CATEG_ID;
                    var PROD_NAME = jsonData[key].PROD_NAME;

                    var edit_prod_cls = "edit-prod";
                    var edit_url = "/coldfusion-fusebox5-test/index.cfm?action=prods.save_form&Prod_id="+PROD_ID;
                    var delete_prod_cls = "delete-prod";
                    var delete_url = "/coldfusion-fusebox5-test/index.cfm?action=prods.delete&Prod_id="+PROD_ID;
                    var view_prod_cls = "delete-prod";
                    var view_url = "/coldfusion-fusebox5-test/index.cfm?action=prods.delete&Prod_id="+PROD_ID;

                    html += "<tr>";
                    html += "<td class='test'>"+PROD_ID+"</td>";
                    html += "<td>"+get_categ_list[CATEG_ID]+"</td>";
                    html += "<td>"+PROD_NAME+"</td>";
                    html += "<td>";
                    html += "<a class='"+edit_prod_cls+"' rel='"+PROD_ID+"' href='"+edit_url+"'>Edit</a> ";
                    html += "<a class='"+delete_prod_cls+"' rel='"+PROD_ID+"' href='"+delete_url+"'>Delete</a> ";
                    html += "<a class='"+view_prod_cls+"' rel='"+PROD_ID+"' href='"+view_url+"'>Details</a> ";
                    html += "</td>";
                    html += "</tr>";
                }

                $('#grid-data-tbody').html(html);
            });
        }
    };

    $(document).on("click", ".delete-prod", function(e) {
            e.preventDefault();
            var Prod_id = this.rel;
            var config = {
                cmp: 'delete-prod-dialog',
                attr: {
                    autoOpen: true,
                    modal: true,
                    width: 400,
                    buttons: [
                        {
                            text: "OK",
                            click: function() {
                                $.prods.deleteRecord(Prod_id);
                                $(this).dialog("close");
                            }
                        },
                        {
                            text: "Cancel",
                            click: function() {
                                $(this).dialog("close");
                            }
                        }
                    ]
                },
                title: 'Delete Data',
                content: 'Are you sure you want to delete prod data?'
            };

            $.dialog.build(config);
            $.dialog.show();

        });
})(jQuery);
