(function($) {
	$.dialog = {
        config_cmp: "system-dialog",
        config_attr: {
            autoOpen: true,
            modal: true,
            height: 200,
            width: 400,
            buttons: [
                {
                    text: "OK",
                    click: function() {
                        $(this).dialog("close");
                    }
                }
            ]
        },
        config_title: "System default dialog",
        config_content: "System default dialog",

		init: function() {
			var me = this;
		},

        build: function(config) {
            var me = this;

            me.config_cmp = (typeof config.cmp == "undefined") ? me.config_cmp : config.cmp;
            me.config_attr = (typeof config.attr == "undefined") ?  me.config_attr : config.attr;
            me.config_title = (typeof config.title == "undefined") ?  me.config_title : config.title;
            me.config_content = (typeof config.content == "undefined") ?  me.config_content : config.content;
        }, 

		show: function() {
            var me = this;

            if ($("#" + me.config_cmp).length == 0) {
                $("body").append("<div id=\"" + me.config_cmp + "\" title=\"" + me.config_title + "\",>" + me.config_content + "</div>");
            }

			$("#" + me.config_cmp).dialog(me.config_attr);
		}
	};
})(jQuery);