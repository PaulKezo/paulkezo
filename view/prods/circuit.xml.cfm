<circuit access="internal">

    <fuseaction name="header">
        <include template="dsp_header.cfm" />
    </fuseaction>

	<fuseaction name="body">
		<include template="dsp_body.cfm" />
	</fuseaction>

    <fuseaction name="footer">
        <include template="dsp_footer.cfm" />
    </fuseaction>

    <fuseaction name="save_form">
        <if condition="NOT isDefined('url.Prod_id')">
            <true><include template="frm_add.cfm" /></true>
            <false><include template="frm_edit.cfm" /></false>
        </if>
    </fuseaction>

    <fuseaction name="view_lp">
        <include template="dsp_view_lp.cfm" />
    </fuseaction>

</circuit>
