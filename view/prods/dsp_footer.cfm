<div id="footer-content">
    <cfset categ_list = structNew() />
    <cfloop from="1" to=#get_categ_list.recordCount# index="i">
        <cfset categ_list[get_categ_list.Category_id[i]] = get_categ_list.Cat_name[i] />
    </cfloop>
    
    <script type="text/javascript" src="<cfoutput>#request.jsPath#</cfoutput>jquery/dialog.js"></script>
    <script type="text/javascript" src="<cfoutput>#request.jsPath#</cfoutput>jquery/prods.js"></script>
    <script type="text/javascript">
        var get_categ_list = <cfoutput>#serializeJSON(categ_list)#</cfoutput>;

        $("#save-prod").submit(function(e) {
            e.preventDefault();
            $.prods.formSubmit();
        });

        $("#search-category").change(function(e) {
            e.preventDefault();

            if ($("#search-category").val() == "") {
                $("#search-keyword").val('')
            }
        });

        $("#search-keyword").change(function(e) {
            e.preventDefault();

            if ($("#search-category").val() == "") {
                $("#search-keyword").val('')
            }
        });

        $("#search-prod").click(function(e) {
            e.preventDefault();
            var keyword = $("#search-keyword").val();
            var category = $("#search-category").val();

            $.prods.searchRecord(category, keyword);
        });

        $("#refresh-prod").click(function(e) {
            e.preventDefault();
            $.prods.reloadSummaryView();
        });

        

      
    </script>
</div>


