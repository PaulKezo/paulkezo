
<div id="body-content" class="col-md-12">
    <div class="col-md-2">
    <select id="search-category" class="form-control" >
        
        <option value="Prod_id">ID</option>
    </select>
    </div>
    <div class="col-md-2">
    <input type="text" id="search-keyword" class="form-control"/>
    </div>

    <a href="#" id="search-prod" class="form-control">Search</a>   
    <table class="table table-hover" >

        <thead>
             <tr><th>ID</th><th>Category</th><th>Product Name</th><th>Product Price</th><th>Controls</th></tr>
        </thead>

        <tbody id="grid-data-tbody">
        <cfloop from="1" to="#get_prods_list_desc.recordCount#" index="i">
            <cfoutput>
                <tr id="#get_prods_list_desc['Prod_id'][i]#">
                    <td class="test">#get_prods_list_desc['Prod_id'][i]#</td>
                    <td>#get_categ_list['Cat_name'][get_prods_list_desc['Categ_id'][i]]#</td>
                    <td>#get_prods_list_desc['Prod_name'][i]#</td>
                    <td>#get_prods_list_desc['Prod_price'][i]#</td>
                    <td>
                        <a class="edit-prod" rel="#get_prods_list_desc['Prod_id'][i]#" href="#mySelf##xfa.show_save_prod_form#&Prod_id=#get_prods_list_desc['Prod_id'][i]#">Edit</a>
                        <a class="delete-prod" rel="#get_prods_list_desc['Prod_id'][i]#" href="#mySelf##xfa.do_delete_prod#&Prod_id=#get_prods_list_desc['Prod_id'][i]#">Delete</a>
                        <a class="view-prod" rel="#get_prods_list_desc['Prod_id'][i]#" href="#mySelf##xfa.do_view_prod#&Prod_id=#get_prods_list_desc['Prod_id'][i]#">Details</a>
                    </td>
                </tr>
            </cfoutput>
        </cfloop>
        </tbody>

    </table>


</div>





 

