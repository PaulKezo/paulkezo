<div id="body-content">
    <div id="error-message"></div>
    <form action="" method="" id="save-prod">
<input type="hidden" name="Prod_id" value="#Prod_id#">
        <div id="save-prod-elements-1">
            <span>Category:</span>
            <select id="categ-id"  class="form-control" name="Categ_id">
                <option value="">----Select----</option>
                <cfloop from="1" to="#get_categ_list.recordCount#" index="i">
                    <cfoutput>
                        <option  value="#get_categ_list['Category_id'][i]#">#get_categ_list['Cat_name'][i]#</option>
                    </cfoutput>
                </cfloop>
            </select>
            <div class="clear-both-10"></div>
        </div>

        <div id="save-prod-elements-2">
            <span>Product Name:</span>
            <input type="text" id="Prod-name" name="Prod_name" value="" class="form-control"/>
            <div class="clear-both-10"></div>
        </div>
         <div id="save-prod-elements-2">
            <span>Product Price:</span>
            <input type="text" id="Prod-price" name="Prod_Price" value="" class="form-control"/>
            <div class="clear-both-10"></div>
        </div>

        <div id="save-prod-elements-button">
            <input type="submit" id="done" value="Done" class="btn btn-default"/>
        </div>
    </form>
</div>