<div id="body-content">
    <table class="table table-hover" >

        <thead>
            <tr>
                <th>ID</th>
                <th>Category</th>
            </tr>
        </thead>

        <tbody>
            <cfloop from="1" to="#get_categ_list.recordCount#" index="i">
                <tr>
                    <td><cfoutput>#get_categ_list['Category_id'][i]#</cfoutput></td>
                    <td><cfoutput>#get_categ_list['Cat_name'][i]#</cfoutput></td>
                </tr>
            </cfloop>
        </tbody>

    </table>
</div>