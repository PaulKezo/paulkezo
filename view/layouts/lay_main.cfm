
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<cfoutput>#request.cssPath#</cfoutput>default.css">
    <link href="<cfoutput>#request.jsPath#</cfoutput>jquery/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet">
    
    <script type="text/javascript" src="<cfoutput>#request.jsPath#</cfoutput>jquery/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="<cfoutput>#request.jsPath#</cfoutput>jquery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
  
        <cfoutput>#header_content#</cfoutput>

    <div id="frame">
        <cfoutput>#body_content#</cfoutput>
        <cfoutput>#footer_content#</cfoutput>
    </div>
   </div>