<circuit access="internal">

    <fuseaction name="buildpage">
        <!-- Set layout -->
        <set name="layout" value="main" overwrite="false" />

        <if condition="(structKeyExists(GetHttpRequestData(),'headers')
            AND structKeyExists(GetHttpRequestData()['headers'],'X-Requested-With')
            AND GetHttpRequestData()['headers']['X-Requested-With'] EQ 'XMLHttpRequest')">
            <true><set name="layout" value="ajax_json" overwrite="true" /></true>
        </if>

        <!-- Load layout -->
        <if condition="layout EQ 'ajax_json'">
            <true><include template="lay_ajax_json.cfm" /></true>
        </if>

        <if condition="layout EQ 'main'">
            <true><include template="lay_main.cfm" /></true>
        </if>

    </fuseaction>

</circuit>
