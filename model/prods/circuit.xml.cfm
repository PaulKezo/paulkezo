<circuit access="internal">

    <fuseaction name="get_prods_desc">
        <include template="act_get_prods_desc.cfm" />
    </fuseaction>

    <fuseaction name="get_prod_by_id">
        <include template="act_get_prod_by_id.cfm" />
    </fuseaction>

    <fuseaction name="get_prod_by_keyword_category">
        <include template="act_get_prod_by_keyword_categ.cfm" />
    </fuseaction>

    <fuseaction name="save_prod">
        <include template="act_save_prod.cfm" />
    </fuseaction>

    <fuseaction name="delete_prod">
        <include template="act_delete_prod.cfm" />
    </fuseaction>

 
 
</circuit>
