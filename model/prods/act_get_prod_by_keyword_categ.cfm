<cfswitch expression="#url.category#">
	<cfcase value="Prod_id">
		<cfset Prod_id = 0 />
		<cfif #keyword# NEQ "" AND isNumeric(url.keyword)>
			<cfset Prod_id = #keyword# />
		</cfif>
	    <cfset get_prod_by_keyword_category = "#application.prodsmanager.GetProdById(Prod_id)#" />
	</cfcase>  

	<cfcase value="Categ_id">
		<cfset Categ_id = 0 />
		<cfset get_categ_by_name = "#application.categsmanager.GetCategByName(url.keyword)#" />
		
		<cfif get_categ_by_name.recordCount NEQ 0>
			<cfset Categ_id = #get_categ_by_name['Category_id'][1]# />
		</cfif>

		<cfset get_prod_by_keyword_category = "#application.prodsmanager.GetProdByCategIdDesc(Categ_id)#" />
	</cfcase>

	<cfcase value="Prod_name">
		<cfset get_prod_by_keyword_category = "#application.prodsmanager.GetProdByProdNameIdDesc(url.keyword)#" />
	</cfcase>

	<cfdefaultcase> 
		<cfset get_prod_by_keyword_category = "#application.prodsmanager.GetProdListDesc()#" />
	</cfdefaultcase> 
</cfswitch>