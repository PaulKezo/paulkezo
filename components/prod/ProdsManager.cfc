
<cfcomponent displayname="ProdsManager">
  

    <cffunction name="Init" access="public" returntype="ProdsManager" output="false" displayname="" hint="">
        <cfreturn this />
    </cffunction>

    <cffunction name="GetProdListDesc">
        <cfquery name="results" datasource="testcf">SELECT * FROM tblproduct ORDER BY Prod_id DESC</cfquery>
        <cfreturn results />
    </cffunction>

    <cffunction name="GetProdById">
        <cfargument name="Prod_id" type="numeric" required="yes">
        <cfquery name="result" datasource="testcf">SELECT * FROM tblproduct WHERE Prod_id=#Prod_id# </cfquery>
        <cfreturn result />
    </cffunction>

    <cffunction name="GetProdByCategIdDesc">
        <cfargument name="Categ_id" type="numeric" required="yes">
        <cfquery name="results" datasource="testcf">SELECT * FROM tblproduct WHERE Categ_id=#Categ_id# ORDER BY Prod_id DESC</cfquery>
        <cfreturn results />
    </cffunction>

    <cffunction name="GetProdByFullNameIdDesc">
        <cfargument name="full_name" type="string" required="yes">
        <cfquery name="results" datasource="testcf">SELECT * FROM tblproduct WHERE Prod_name LIKE '%#Prod_name#%' ORDER BY Prod_id DESC</cfquery>
        <cfreturn results />
    </cffunction>

    <cffunction name="SaveProd">
        <cfargument name="form_data" type="struct" required="yes">
       <cfif NOT StructKeyExists(form_data, 'Category_id')>
            <cfquery name="" datasource="testcf" result="result">
                INSERT INTO tblproduct VALUES ( #form_data['Prod_Price']#,#form_data['Categ_id']#,'#form_data['Prod_name']#')
            </cfquery>
        <cfelse>
            <cfquery name="" datasource="testcf" result="result">
                UPDATE tblproduct SET Prod_price=#form_data['Prod_Price']#,Categ_id='#form_data['Categ_id']#', Prod_name='#form_data['Prod_name']#' WHERE Prod_id=#form_data['Prod_id']#
            </cfquery>
        </cfif>
        <cfreturn result />
    </cffunction>

    <cffunction name="DeleteProd">
        <cfargument name="Prod_id" type="numeric" required="yes">
        <cfquery name="deleteProd" datasource="testcf" result="result">DELETE FROM tblproduct WHERE Prod_id=#Prod_id#</cfquery>
    </cffunction>

</cfcomponent>
