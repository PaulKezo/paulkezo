
<cfcomponent displayname="CategsManager">


    <cffunction name="Init" access="public" returntype="CategsManager" output="false" displayname="" hint="">
        <cfreturn this />
    </cffunction>

    <cffunction name="GetCategList">
        <cfquery name="results" datasource="testcf">SELECT * FROM tblcategory</cfquery>
        <cfreturn results />
    </cffunction>

    <cffunction name="GetCategByName">
        <cfargument name="name" type="string">
        <cfquery name="results" datasource="testcf">SELECT * FROM tblcategory WHERE Cat_name='#Cat_name#'</cfquery>
        <cfreturn results />
    </cffunction>


</cfcomponent>
