<!---
  Created by Mark Dujali on 7/28/2015.
--->
<cfcomponent displayname="DataFormats">
   <!--- Place your content here --->

    <cffunction name="Init" access="public" returntype="DataFormats" output="false" displayname="" hint="">
        <cfreturn this>
    </cffunction>

    <cffunction name="QueryToArrayOfStructures" access="public" returntype="array" output="false" hint="I converts a query object into an array of structures.">
        <cfargument name="argQuery" type="query" required="true" hint="The query to be transformed." />
        <cfset var stLocal = structNew() />

        <cfscript>
            stLocal.aReturn = arrayNew(1);

            stLocal.cols = listToArray(arguments.argQuery.columnlist);
            stLocal.row = 1;
            stLocal.thisRow = "";
            stLocal.col = 1;
            for(stLocal.row = 1; stLocal.row lte arguments.argQuery.recordcount; stLocal.row = stLocal.row + 1) {
                stLocal.thisRow = structNew();
                for(stLocal.col = 1; stLocal.col lte arrayLen(stLocal.cols); stLocal.col = stLocal.col + 1) {
                    stLocal.thisRow[stLocal.cols[stLocal.col]] = arguments.argQuery[stLocal.cols[stLocal.col]][stLocal.row];
                }
                arrayAppend(stLocal.aReturn, duplicate(stLocal.thisRow));
            }
            return(stLocal.aReturn);
        </cfscript>
    </cffunction>

</cfcomponent>
